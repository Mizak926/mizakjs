var Tools = (function () {
  var mzk = {};


  /*************
  MATHS
  **************/
  function clamp (value, min, max){
      return Math.min(Math.max(value, min), max);
  }

  function randomBetween (min, max) {
    return Math.random() * (max - min) + min;
  }

  /*************
  DOM
  **************/

  function prependChild (newNode, nodeParent) {
    nodeParent.insertBefore(newNode, nodeParent.firstChild);
  }

  /*
  SOURCE : http://stackoverflow.com/a/384380/2549654
  */
  function isNode(o){
    return (
      typeof Node === "object" ? o instanceof Node :
      o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName==="string"
    );
  }

  function isElement(o){
    return (
      typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
      o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string"
    );
  }

  /*
  SOURCE : http://www.kirupa.com/html5/animating_movement_smoothly_using_css.htm
  */
  var transform = ["transform", "msTransform", "webkitTransform", "mozTransform", "oTransform"];
  function getSupportedPropertyName() {
    for (var i = 0; i < transform.length; i++) {
        if (typeof document.body.style[transform[i]] != "undefined") {
            return transform[i];
        }
    }
    return null;
  }

  /**
     * Get the closest element of a given element by class
     *
     * Take an element (the firt param), and traverse the DOM upward from it
     * untill it hits the element with a given class name (second parameter).
     * This mimics jquery's `.closest()`.
     *
     * @param  {element} el    The element to start from
     * @param  {string}  clazz The class name
     * @return {element}       The closest element
    */
  // https://gist.github.com/matesnippets/5cf80e054cb574f03215
  function closest(el, clazz) {
    while (el.className != clazz) {
      el = el.parentNode;
      if (!el) {
        return null;
      }
    }
    return el;
  }

  /*
  SOURCE : http://stackoverflow.com/questions/6157929/how-to-simulate-a-mouse-click-using-javascript
  */
  function simulateEvent(element, eventName)
  {
    var options = extend(defaultOptions, arguments[2] || {});
    var oEvent, eventType = null;

    for (var name in eventMatchers)
    {
        if (eventMatchers[name].test(eventName)) { eventType = name; break; }
    }

    if (!eventType)
        throw new SyntaxError('Only HTMLEvents and MouseEvents interfaces are supported');

    if (document.createEvent)
    {
        oEvent = document.createEvent(eventType);
        if (eventType == 'HTMLEvents')
        {
            oEvent.initEvent(eventName, options.bubbles, options.cancelable);
        }
        else
        {
            oEvent.initMouseEvent(eventName, options.bubbles, options.cancelable, document.defaultView,
            options.button, options.pointerX, options.pointerY, options.pointerX, options.pointerY,
            options.ctrlKey, options.altKey, options.shiftKey, options.metaKey, options.button, element);
        }
        element.dispatchEvent(oEvent);
    }
    else
    {
        options.clientX = options.pointerX;
        options.clientY = options.pointerY;
        var evt = document.createEventObject();
        oEvent = extend(evt, options);
        element.fireEvent('on' + eventName, oEvent);
    }
    return element;
  }

  function extend(destination, source) {
    for (var property in source)
      destination[property] = source[property];
    return destination;
  }

  var eventMatchers = {
      'HTMLEvents': /^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,
      'MouseEvents': /^(?:click|dblclick|mouse(?:down|up|over|move|out|leave|enter))$/
  }
  var defaultOptions = {
      pointerX: 0,
      pointerY: 0,
      button: 0,
      ctrlKey: false,
      altKey: false,
      shiftKey: false,
      metaKey: false,
      bubbles: true,
      cancelable: true
  }

  /*
  SOURCE : http://stackoverflow.com/a/24256605/2549654
  */
  function addEvent( obj, type, fn, bubble) {
    bubble ? bubble : false
    if ( obj.attachEvent ) {
      obj['e'+type+fn] = fn;
      obj[type+fn] = function(){obj['e'+type+fn]( window.event );}
      obj.attachEvent( 'on'+type, obj[type+fn] );
    } else
      obj.addEventListener( type, fn, bubble );
  }

  function removeEvent( obj, type, fn, bubble) {
    bubble ? bubble : false
    if ( obj.detachEvent ) {
      obj.detachEvent( 'on'+type, obj[type+fn] );
      obj[type+fn] = null;
    } else
      obj.removeEventListener( type, fn, bubble );
  }



  /*************
  ARRAYS
  **************/
  /*
  SOURCE : http://sroucheray.org/blog/2009/11/array-sort-should-not-be-used-to-shuffle-an-array/
  */
  function shuffle(arr){
    var i = arr.length, j, temp;
    if ( i == 0 ) return;
    while ( --i ) {
        j = Math.floor( Math.random() * ( i + 1 ) );
        temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
    return arr;
  };


  /*
  METHODS
  */
  mzk.clamp = clamp;
  mzk.randomBetween = randomBetween;
  mzk.prependChild = prependChild;
  mzk.closest = closest;

  mzk.simulateEvent = simulateEvent;
  mzk.isNode = isNode;
  mzk.isElement = isElement;
  mzk.getSupportedPropertyName = getSupportedPropertyName;
  mzk.addEvent = addEvent;
  mzk.removeEvent = removeEvent;

  mzk.shuffle = shuffle;

  return mzk;
}());


