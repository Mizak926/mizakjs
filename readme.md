#Mizak.js


###Introduction

A substitution to jQuery.

### Functions

  - Maths
  	- clamp()
  	- randomBetween()
  - DOM Manipulation
  	- isNode()
  	- isElement()
  	- simulateEvent()
  	- prependChild()
  	- getSupportedPropertyName()
    - closest()
  - DOM Events
  	- addEvent()
  	- removeEvent()
  - Arrays
  	- shuffle()


###ROADMAP

  - Improve documentation
  - add exemples
  - improve readme.md with anchor
  - add polyfills
  - add `querySelector` function